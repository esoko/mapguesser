#!/bin/bash

ROOT_DIR=$(dirname $(readlink -f "$0"))/..

. ${ROOT_DIR}/.env

echo "Installing Composer packages..."
if [ -z "${DEV}" ] || [ "${DEV}" -eq "0" ]; then
    (cd ${ROOT_DIR} && composer install --no-dev)
else
    (cd ${ROOT_DIR} && composer install --dev)
fi

echo "Installing Yarn packages..."
(cd ${ROOT_DIR}/public/static && yarn install)

echo "Migrating DB..."
(cd ${ROOT_DIR} && ./mapg db:migrate)

if [ -z "${DEV}" ] || [ "${DEV}" -eq "0" ]; then
    echo "Minifying JS, CSS and SVG files..."
    ${ROOT_DIR}/scripts/minify.sh

    echo "Linking view files..."
    (cd ${ROOT_DIR} && ./mapg view:link)
fi
