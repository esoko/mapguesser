UPDATE `sessions` SET id=SUBSTRING(id, 1, 32);

ALTER TABLE `sessions`
  MODIFY `id` varchar(32) CHARACTER SET ascii NOT NULL;
