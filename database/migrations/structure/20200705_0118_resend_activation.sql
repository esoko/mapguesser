UPDATE `user_confirmations` SET token=SUBSTRING(token, 1, 32);

ALTER TABLE `user_confirmations`
  ADD `last_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  MODIFY `token` varchar(32) CHARACTER SET ascii NOT NULL;
