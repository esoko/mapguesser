ALTER TABLE
  `places`
ADD
  `pov_heading` decimal(6, 3) NOT NULL DEFAULT 0.0,
ADD
  `pov_pitch` decimal(5, 3) NOT NULL DEFAULT 0.0,
ADD
  `pov_zoom` decimal(5, 4) NOT NULL DEFAULT 0.0;
