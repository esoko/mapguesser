var Account = {
    original: null,
    countdown: null,

    openGoogleAuthenticate: function () {
        window.open('/account/googleAuthenticate', 'googleAuthenticate', 'height=600,width=600')
    },

    authenticatedWithGoogleCallback: function (authenticatedWithGoogleUntil) {
        var password = document.getElementsByTagName('form')[0].elements.password;
        var button = document.getElementById('authenticateWithGoogleButton');

        Account.original = {
            type: password.type,
            placeholder: password.placeholder,
            required: password.required,
            disabled: password.disabled
        };

        password.type = 'text';
        password.placeholder = 'Authenticated!'
        password.value = '';
        password.required = false;
        password.disabled = true;

        button.disabled = true;

        Account.countdown = setInterval(function () {
            var timeLeft = Math.ceil((authenticatedWithGoogleUntil.getTime() - new Date().getTime()) / 1000);

            if (timeLeft > 30) {
                return;
            }

            if (timeLeft <= 0) {
                Account.resetGoogleAuthentication();
                return;
            }

            password.placeholder = 'Authenticated! ' + timeLeft + ' seconds left...';
        }, 1000);
    },

    resetGoogleAuthentication: function () {
        if (Account.countdown !== null) {
            clearInterval(Account.countdown);
        }

        var password = document.getElementsByTagName('form')[0].elements.password;
        var button = document.getElementById('authenticateWithGoogleButton');

        password.type = Account.original.type;
        password.placeholder = Account.original.placeholder
        password.required = Account.original.required;
        password.disabled = Account.original.disabled;

        button.disabled = false;
    }
};

(function () {
    document.getElementById('authenticateWithGoogleButton').onclick = function () {
        Account.openGoogleAuthenticate();
    };

    document.getElementsByTagName('form')[0].onreset = function () {
        Account.resetGoogleAuthentication();
    };
})();
