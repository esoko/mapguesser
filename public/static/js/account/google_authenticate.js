(function () {
    if (success) {
        window.opener.Account.authenticatedWithGoogleCallback(authenticatedWithGoogleUntil);
        window.close();
    } else {
        document.getElementById('closeWindowButton').onclick = function () {
            window.close();
        }
    }
})();
