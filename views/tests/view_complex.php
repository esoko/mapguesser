@css(test1.css)
@js(test1.js)
@js(test2_<?= $some['expression'] ?>)

@extra
<?php phpinfo() ?>
@endextra

@extends(parent)

@section(section1)
<div>Test HTML with @extends - section 1</div>
@endsection

@section(section2)
<div>Test HTML with @extends - section 2</div>
@endsection

@extra
<?php $a = 'test_string' ?>
EXTRA
@endextra
