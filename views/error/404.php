@extends(templates/layout_normal)

@section(main)
    <h2>404 | Page not found</h2>
    <p>The requested URL was not found on this server. <a href="/" title="<?= $_ENV['APP_NAME'] ?>">Back to start.</a></p>
@endsection
