@extends(templates/layout_normal)

@section(main)
    <h2>Login up with Google</h2>
    <div class="box">
        <p class="error justify">Authentication with Google failed. Please <a href="/login/google" title="Login with Google">try again</a>!</p>
    </div>
@endsection
