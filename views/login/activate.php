@extends(templates/layout_normal)

@section(main)
    <h2>Account activation</h2>
    <div class="box">
        <p class="error justify">Activation failed. Please check the link you entered, or retry <a href="/signup" title="Sign up">signing up</a>!</p>
    </div>
@endsection
