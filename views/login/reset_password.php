@extends(templates/layout_normal)

@section(main)
    <h2>Reset password</h2>
    <div class="box">
        <?php if ($success) : ?>
            <form id="resetPasswordForm" action="/password/reset/<?= $token ?>" method="post" data-redirect-on-success="/">
                <input class="big fullWidth" type="email" name="email" placeholder="Email address" value="<?= $email ?>" disabled>
                <input class="big fullWidth marginTop" type="password" name="password" placeholder="Password" required minlength="6" autofocus>
                <input class="big fullWidth marginTop" type="password" name="password_confirm" placeholder="Password confirmation" required minlength="6">
                <p id="resetPasswordFormError" class="formError justify marginTop"></p>
                <div class="right">
                    <button class="marginTop" type="submit">Reset password</button>
                </div>
            </form>
        <?php else: ?>
            <p class="error justify">Confirming your identity failed. Please check the link you entered, or retry <a href="/password/requestReset" title="Request password reset">requesting password reset</a>!</p>
        <?php endif; ?>
    </div>
@endsection
