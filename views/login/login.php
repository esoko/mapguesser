@extends(templates/layout_normal)

@section(main)
    <h2>Login</h2>
    <div class="box">
        <form id="loginForm" action="/login" method="post" data-redirect-on-success="/">
            <input class="big fullWidth" type="email" name="email" placeholder="Email address" required autofocus>
            <input class="big fullWidth marginTop" type="password" name="password" placeholder="Password" required minlength="6">
            <p id="loginFormError" class="formError justify marginTop"></p>
            <div class="right marginTop">
                <button type="submit">Login</button>
            </div>
            <p class="center marginTop"><a href="/password/requestReset" title="Request password reset">Forgot your password?</a></p>
            <p class="center marginTop"><a href="/signup" title="Sign up">New to <?= $_ENV['APP_NAME'] ?>?</a></p>
            <hr>
            <div class="center">
                <a class="button yellow" href="/login/google" title="Login with Google">Login with Google</a>
            </div>
        </form>
    </div>
@endsection
