@extends(templates/layout_normal)

@section(main)
    <h2>Sign up</h2>
    <div class="box">
        <p class="justify">Sign up was successful. Please check your email and click on the activation link to activate your account!</p>
    </div>
@endsection
