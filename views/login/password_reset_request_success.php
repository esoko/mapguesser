@extends(templates/layout_normal)

@section(main)
    <h2>Request password reset</h2>
    <div class="box">
        <p class="justify">Password reset was successfully requested. Please check your email and click on the link to reset your password!</p>
    </div>
@endsection
