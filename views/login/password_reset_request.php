@extends(templates/layout_normal)

@section(main)
    <h2>Request password reset</h2>
    <div class="box">
        <form id="passwordResetForm" action="/password/requestReset" method="post" data-redirect-on-success="/password/requestReset/success">
            <input class="big fullWidth" type="email" name="email" placeholder="Email address" value="<?= isset($email) ? $email : '' ?>" required autofocus>
            <p id="passwordResetFormError" class="formError justify marginTop"></p>
            <div class="right marginTop">
                <button type="submit">Continue</button>
            </div>
        </form>
    </div>
@endsection
