@extends(templates/mapguesser)

@section(content)
<header class="small">
    <h1>
        <a href="/" title="<?= $_ENV['APP_NAME'] ?>">
            <img class="inline" width="1em" height="1em" src="<?= $_ENV['STATIC_ROOT'] ?>/img/icon.svg?rev=<?= REVISION ?>" alt="<?= $_ENV['APP_NAME'] ?>"><!--
         --><span><?= $_ENV['APP_NAME'] ?></span>
        </a>
    </h1>
    <p class="header">
        @yields('subheader')
    </p>
</header>
<main class="full">
@yields('main')
</main>
@endsection
