<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="<?= $_ENV['APP_NAME'] ?> – A game about guessing where you are based on a street view panorama.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#198231">
    <title><?= $_ENV['APP_NAME'] ?></title>
    <?php if (preg_match('/^(http(s)?:)?\/\//', $_ENV['STATIC_ROOT']) === 1): ?>
        <link href="<?= $_ENV['STATIC_ROOT'] ?>" rel="preconnect">
    <?php endif; ?>
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://maps.googleapis.com" rel="preconnect">
    <?php if (!empty($_ENV['GOOGLE_ANALITICS_ID'])): ?>
        <link href="https://www.googletagmanager.com" rel="preconnect">
        <link href="https://www.google-analytics.com" rel="preconnect">
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500&amp;display=swap" rel="stylesheet">
    <link href="<?= $_ENV['STATIC_ROOT'] ?>/css/mapguesser.css?rev=<?= REVISION ?>" rel="stylesheet">
    @yields('externalCss')
    @yields('inlineCss')
    <link rel="icon" type="image/png" sizes="192x192" href="<?= $_ENV['STATIC_ROOT'] ?>/img/favicon/192x192.png?rev=<?= REVISION ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $_ENV['STATIC_ROOT'] ?>/img/favicon/96x96.png?rev=<?= REVISION ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $_ENV['STATIC_ROOT'] ?>/img/favicon/32x32.png?rev=<?= REVISION ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $_ENV['STATIC_ROOT'] ?>/img/favicon/16x16.png?rev=<?= REVISION ?>">
</head>
<body>
    <div id="loading">
        <img src="<?= $_ENV['STATIC_ROOT'] ?>/img/loading.svg?rev=<?= REVISION ?>" width="64" height="64" alt="Loading">
    </div>
    <div id="cover"></div>
    <div id="modal" class="modal">
        <h2 id="modalTitle"></h2>
        <p id="modalText" class="justify marginTop"></p>
        <div id="modalButtons" class="right"></div>
    </div>
    @yields('pagemodal')
    @yields('content')
    <?php if (!isset($_COOKIE['COOKIES_CONSENT'])): ?>
        <div id="cookiesNotice">
            <p class="small">
                <?= $_ENV['APP_NAME'] ?> uses cookies to improve user experience. By using the app or clicking 'Agree', you consent to our use of cookies.
            </p>
            <button id="agreeCookiesButton" class="small marginTop">Agree</button>
        </div>
    <?php endif; ?>
    <script>
        const STATIC_ROOT = '<?= $_ENV['STATIC_ROOT'] ?>';
        const REVISION = '<?= REVISION ?>';
        var ANTI_CSRF_TOKEN = '<?= \Container::$request->session()->get('anti_csrf_token') ?>';
        const GOOGLE_MAPS_JS_API_KEY = '<?= $_ENV['GOOGLE_MAPS_JS_API_KEY'] ?>';
        <?php if (!empty($_ENV['GOOGLE_ANALITICS_ID'])): ?>
            const GOOGLE_ANALITICS_ID = '<?= $_ENV['GOOGLE_ANALITICS_ID'] ?>';
        <?php endif; ?>
        const COOKIES_CONSENT = <?= isset($_COOKIE['COOKIES_CONSENT']) ? 'true' : 'false' ?>;
    </script>
    @yields('pageScript')
    <script src="<?= $_ENV['STATIC_ROOT'] ?>/js/mapguesser.js?rev=<?= REVISION ?>"></script>
    @yields('externalJs')
    @yields('inlineJs')
</body>
</html>
