@extends(templates/mapguesser)

@section(content)
<header>
    <h1>
        <img class="inline" width="1em" height="1em" src="<?= $_ENV['STATIC_ROOT'] ?>/img/icon.svg?rev=<?= REVISION ?>" alt="<?= $_ENV['APP_NAME'] ?>"><!--
     --><?= $_ENV['APP_NAME'] ?>
    </h1>
</header>
<main>
@yields('main')
</main>
@endsection
