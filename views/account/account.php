@js(js/account/account.js)

@extends(templates/layout_normal)

@section(main)
    <h2>Account</h2>
    <div class="box">
        <form id="accountForm" action="/account" method="post" data-observe-inputs="password_new,password_new_confirm">
            <?php if ($user['password'] !== null && $user['google_sub'] !== null): ?>
                <p class="justify small">Please confirm your identity with your password or with Google to modify your account.</p>
                <div class="inputWithButton">
                    <input type="password" name="password" placeholder="Current password" required minlength="6" autofocus><!--
                 --><button id="authenticateWithGoogleButton" class="yellow" type="button">Google</button>
                </div>
            <?php elseif ($user['password'] !== null): ?>
                <p class="justify small">Please confirm your identity with your password to modify your account.</p>
                <input class="big fullWidth" type="password" name="password" placeholder="Current password" required minlength="6" autofocus>
            <?php elseif ($user['google_sub'] !== null): ?>
                <p class="justify small">Please confirm your identity with Google to modify your account.</p>
                <div class="inputWithButton">
                    <input type="text" name="password" placeholder="Authenticate with Google..." disabled><!--
                 --><button id="authenticateWithGoogleButton" class="yellow" type="button">Google</button>
                </div>
            <?php endif; ?>
            <hr>
            <?php /* TODO: disabled for the time being, email modification should be implemented */ ?>
            <input class="big fullWidth" type="email" name="email" placeholder="Email address" value="<?= $user['email'] ?>" disabled>
            <input class="big fullWidth marginTop" type="password" name="password_new" placeholder="New password" minlength="6">
            <input class="big fullWidth marginTop" type="password" name="password_new_confirm" placeholder="New password confirmation" minlength="6">
            <p id="accountFormError" class="formError justify marginTop"></p>
            <div class="right marginTop">
                <button type="submit" name="submit" disabled>Save</button>
            </div>
            <hr>
            <div class="center">
                <a class="button red" href="/account/delete" title="Delete account">Delete account</a>
            </div>
        </form>
    </div>
@endsection
