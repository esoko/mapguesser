@js(js/account/account.js)

@extends(templates/layout_normal)

@section(main)
    <h2>Delete account</h2>
    <div class="box">
        <form id="deleteAccountForm" action="/account/delete" method="post" data-redirect-on-success="/">
            <p class="justify marginBottom">Are you sure you want to delete your account? This cannot be undone!</p>
            <?php if ($user['password'] !== null && $user['google_sub'] !== null): ?>
                <p class="justify small">Please confirm your identity with your password or with Google to delete your account.</p>
                <div class="inputWithButton">
                    <input type="password" name="password" placeholder="Current password" required minlength="6" autofocus><!--
                 --><button id="authenticateWithGoogleButton" class="yellow" type="button">Google</button>
                </div>
            <?php elseif ($user['password'] !== null): ?>
                <p class="justify small">Please confirm your identity with your password to delete your account.</p>
                <input class="big fullWidth" type="password" name="password" placeholder="Current password" required minlength="6" autofocus>
            <?php elseif ($user['google_sub'] !== null): ?>
                <p class="justify small">Please confirm your identity with Google to delete your account.</p>
                <div class="inputWithButton">
                    <input type="text" name="password" placeholder="Authenticate with Google..." disabled><!--
                 --><button id="authenticateWithGoogleButton" class="yellow" type="button">Google</button>
                </div>
            <?php endif; ?>
            <p id="deleteAccountFormError" class="formError justify marginTop"></p>
            <div class="right marginTop">
                <button class="red marginRight" type="submit" name="submit">Delete account</button><!--
             --><a class="button gray marginTop" href="/account" title="Back to account">Cancel</a>
            </div>
        </form>
    </div>
@endsection
