@js(js/account/google_authenticate.js)

@extends(templates/layout_minimal)

@section(main)
    <h2>Authenticate with Google</h2>
    <?php if (!$success): ?>
    <div class="box">
        <p class="error justify">
            <?php if (isset($errorText)): ?>
                <?= $errorText ?>
            <?php else: ?>
                Authentication with Google failed.
            <?php endif; ?>
            Please <a id="closeWindowButton" href="javascript:;" title="Close">close this window/tab</a> and try again!
        </p>
    </div>
    <?php endif; ?>
@endsection

@section(pageScript)
<script>
    var success = <?= $success ? 'true' : 'false' ?>;
    <?php if (isset($authenticatedWithGoogleUntil)): ?>
        var authenticatedWithGoogleUntil = new Date('<?= $authenticatedWithGoogleUntil->format('c') ?>');
    <?php endif; ?>
</script>
@endsection
