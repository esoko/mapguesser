<?php namespace MapGuesser\Controller;

use DateInterval;
use DateTime;
use MapGuesser\Http\Request;
use MapGuesser\Interfaces\Request\IRequest;
use MapGuesser\Interfaces\Response\IContent;
use MapGuesser\Interfaces\Response\IRedirect;
use MapGuesser\Mailing\Mail;
use MapGuesser\OAuth\GoogleOAuth;
use MapGuesser\PersistentData\Model\User;
use MapGuesser\PersistentData\Model\UserConfirmation;
use MapGuesser\PersistentData\Model\UserPasswordResetter;
use MapGuesser\PersistentData\PersistentDataManager;
use MapGuesser\Repository\UserConfirmationRepository;
use MapGuesser\Repository\UserPasswordResetterRepository;
use MapGuesser\Repository\UserRepository;
use MapGuesser\Response\HtmlContent;
use MapGuesser\Response\JsonContent;
use MapGuesser\Response\Redirect;
use MapGuesser\Util\JwtParser;

class LoginController
{
    private IRequest $request;

    private PersistentDataManager $pdm;

    private UserRepository $userRepository;

    private UserConfirmationRepository $userConfirmationRepository;

    private UserPasswordResetterRepository $userPasswordResetterRepository;

    public function __construct(IRequest $request)
    {
        $this->request = $request;
        $this->pdm = new PersistentDataManager();
        $this->userRepository = new UserRepository();
        $this->userConfirmationRepository = new UserConfirmationRepository();
        $this->userPasswordResetterRepository = new UserPasswordResetterRepository();
    }

    public function getLoginForm()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        return new HtmlContent('login/login');
    }

    public function getGoogleLoginRedirect(): IRedirect
    {
        $state = bin2hex(random_bytes(16));
        $nonce = bin2hex(random_bytes(16));

        $this->request->session()->set('oauth_state', $state);
        $this->request->session()->set('oauth_nonce', $nonce);

        $oAuth = new GoogleOAuth(new Request());
        $url = $oAuth->getDialogUrl(
            $state,
            $this->request->getBase() . '/' . \Container::$routeCollection->getRoute('login-google-action')->generateLink(),
            $nonce
        );

        return new Redirect($url, IRedirect::TEMPORARY);
    }

    public function getSignupForm()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        if ($this->request->session()->has('tmp_user_data')) {
            $tmpUserData = $this->request->session()->get('tmp_user_data');

            $data = ['email' => $tmpUserData['email']];
        } else {
            $data = [];
        }

        return new HtmlContent('login/signup', $data);
    }

    public function getSignupSuccess(): IContent
    {
        return new HtmlContent('login/signup_success');
    }

    public function getSignupWithGoogleForm()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        if (!$this->request->session()->has('google_user_data')) {
            return new Redirect(\Container::$routeCollection->getRoute('login-google')->generateLink(), IRedirect::TEMPORARY);
        }

        $userData = $this->request->session()->get('google_user_data');

        $user = $this->userRepository->getByEmail($userData['email']);

        return new HtmlContent('login/google_signup', ['found' => $user !== null, 'email' => $userData['email']]);
    }

    public function getRequestPasswordResetForm()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        return new HtmlContent('login/password_reset_request', ['email' => $this->request->query('email')]);
    }

    public function getRequestPasswordResetSuccess(): IContent
    {
        return new HtmlContent('login/password_reset_request_success');
    }

    public function getResetPasswordForm()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        $token = $this->request->query('token');
        $resetter = $this->userPasswordResetterRepository->getByToken($token);

        if ($resetter === null || $resetter->getExpiresDate() < new DateTime()) {
            return new HtmlContent('login/reset_password', ['success' => false]);
        }

        $user = $this->userRepository->getById($resetter->getUserId());

        return new HtmlContent('login/reset_password', ['success' => true, 'token' => $token, 'email' => $user->getEmail()]);
    }

    public function login(): IContent
    {
        if ($this->request->user() !== null) {
            return new JsonContent(['success' => true]);
        }

        $user = $this->userRepository->getByEmail($this->request->post('email'));

        if ($user === null) {
            if (strlen($this->request->post('password')) < 6) {
                return new JsonContent([
                    'error' => [
                        'errorText' => 'The given password is too short. Please choose a password that is at least 6 characters long!'
                    ]
                ]);
            }

            $tmpUser = new User();
            $tmpUser->setPlainPassword($this->request->post('password'));

            $this->request->session()->set('tmp_user_data', [
                'email' => $this->request->post('email'),
                'password_hashed' => $tmpUser->getPassword()
            ]);

            return new JsonContent([
                'redirect' => [
                    'target' => '/' . \Container::$routeCollection->getRoute('signup')->generateLink()
                ]
            ]);
        }

        if (!$user->getActive()) {
            $this->resendConfirmationEmail($user);

            return new JsonContent([
                'error' => [
                    'errorText' => 'User found with the given email address, but the account is not activated. ' .
                        'Please check your email and click on the activation link!'
                ]
            ]);
        }

        if (!$user->checkPassword($this->request->post('password'))) {
            return new JsonContent([
                'error' => [
                    'errorText' => 'The given password is wrong. You can <a href="/password/requestReset?email=' .
                        urlencode($user->getEmail()) . '" title="Request password reset">request password reset</a>!'
                ]
            ]);
        }

        $this->request->setUser($user);

        return new JsonContent(['success' => true]);
    }

    public function loginWithGoogle()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        if ($this->request->query('state') !== $this->request->session()->get('oauth_state')) {
            return new HtmlContent('login/google_login');
        }

        $oAuth = new GoogleOAuth(new Request());
        $tokenData = $oAuth->getToken(
            $this->request->query('code'),
            $this->request->getBase() . '/' . \Container::$routeCollection->getRoute('login-google-action')->generateLink()
        );

        if (!isset($tokenData['id_token'])) {
            return new HtmlContent('login/google_login');
        }

        $jwtParser = new JwtParser($tokenData['id_token']);
        $idToken = $jwtParser->getPayload();

        if ($idToken['nonce'] !== $this->request->session()->get('oauth_nonce')) {
            return new HtmlContent('login/google_login');
        }

        if (!$idToken['email_verified']) {
            return new HtmlContent('login/google_login');
        }

        $user = $this->userRepository->getByGoogleSub($idToken['sub']);

        if ($user === null) {
            $this->request->session()->set('google_user_data', ['sub' => $idToken['sub'], 'email' => $idToken['email']]);

            return new Redirect(\Container::$routeCollection->getRoute('signup-google')->generateLink(), IRedirect::TEMPORARY);
        }

        $this->request->setUser($user);

        return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
    }

    public function logout(): IRedirect
    {
        $this->request->setUser(null);

        return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
    }

    public function signup(): IContent
    {
        if ($this->request->user() !== null) {
            return new JsonContent(['redirect' => ['target' => '/' . \Container::$routeCollection->getRoute('home')->generateLink()]]);
        }

        $user = $this->userRepository->getByEmail($this->request->post('email'));

        if ($user !== null) {
            if ($user->getActive()) {
                if (!$user->checkPassword($this->request->post('password'))) {
                    return new JsonContent([
                        'error' => [
                            'errorText' => 'There is a user already registered with the given email address, ' .
                                'but the given password is wrong. You can <a href="/password/requestReset?email=' .
                                urlencode($user->getEmail()) . '" title="Request password reset">request password reset</a>!'
                        ]
                    ]);
                }

                $this->request->setUser($user);

                $data = ['redirect' => ['target' => '/' . \Container::$routeCollection->getRoute('index')->generateLink()]];
            } else {
                $data = [
                    'error' => [
                        'errorText' => 'There is a user already registered with the given email address. ' .
                            'Please check your email and click on the activation link!'
                    ]
                ];
            }
            return new JsonContent($data);
        }

        if (filter_var($this->request->post('email'), FILTER_VALIDATE_EMAIL) === false) {
            return new JsonContent(['error' => ['errorText' => 'The given email address is not valid.']]);
        }

        if ($this->request->session()->has('tmp_user_data')) {
            $tmpUserData = $this->request->session()->get('tmp_user_data');

            $tmpUser = new User();
            $tmpUser->setPassword($tmpUserData['password_hashed']);

            if (!$tmpUser->checkPassword($this->request->post('password'))) {
                return new JsonContent(['error' => ['errorText' => 'The given passwords do not match.']]);
            }
        } else {
            if (strlen($this->request->post('password')) < 6) {
                return new JsonContent([
                    'error' => [
                        'errorText' => 'The given password is too short. Please choose a password that is at least 6 characters long!'
                    ]
                ]);
            }

            if ($this->request->post('password') !== $this->request->post('password_confirm')) {
                return new JsonContent(['error' => ['errorText' => 'The given passwords do not match.']]);
            }
        }

        $user = new User();
        $user->setEmail($this->request->post('email'));
        $user->setPlainPassword($this->request->post('password'));
        $user->setCreatedDate(new DateTime());

        \Container::$dbConnection->startTransaction();

        $this->pdm->saveToDb($user);

        $token = bin2hex(random_bytes(16));

        $confirmation = new UserConfirmation();
        $confirmation->setUser($user);
        $confirmation->setToken($token);
        $confirmation->setLastSentDate(new DateTime());

        $this->pdm->saveToDb($confirmation);

        \Container::$dbConnection->commit();

        $this->sendConfirmationEmail($user->getEmail(), $token, $user->getCreatedDate());

        $this->request->session()->delete('tmp_user_data');

        return new JsonContent(['success' => true]);
    }

    public function signupWithGoogle(): IContent
    {
        if ($this->request->user() !== null) {
            return new JsonContent(['success' => true]);
        }

        $userData = $this->request->session()->get('google_user_data');

        $user = $this->userRepository->getByEmail($userData['email']);

        if ($user === null) {
            $sendWelcomeEmail = true;

            $user = new User();
            $user->setEmail($userData['email']);
            $user->setCreatedDate(new DateTime());
        } else {
            $sendWelcomeEmail = false;
        }

        $user->setActive(true);
        $user->setGoogleSub($userData['sub']);

        $this->pdm->saveToDb($user);

        if ($sendWelcomeEmail) {
            $this->sendWelcomeEmail($user->getEmail());
        }

        $this->request->session()->delete('google_user_data');
        $this->request->setUser($user);

        return new JsonContent(['success' => true]);
    }

    public function resetSignup(): IContent
    {
        $this->request->session()->delete('tmp_user_data');

        return new JsonContent(['success' => true]);
    }

    public function resetGoogleSignup(): IContent
    {
        $this->request->session()->delete('google_user_data');

        return new JsonContent(['success' => true]);
    }

    public function activate()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        $confirmation = $this->userConfirmationRepository->getByToken(substr($this->request->query('token'), 0, 32));

        if ($confirmation === null) {
            return new HtmlContent('login/activate');
        }

        \Container::$dbConnection->startTransaction();

        $this->pdm->deleteFromDb($confirmation);

        $user = $this->userRepository->getById($confirmation->getUserId());
        $user->setActive(true);

        $this->pdm->saveToDb($user);

        \Container::$dbConnection->commit();

        $this->request->setUser($user);

        return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
    }

    public function cancel()
    {
        if ($this->request->user() !== null) {
            return new Redirect(\Container::$routeCollection->getRoute('index')->generateLink(), IRedirect::TEMPORARY);
        }

        $confirmation = $this->userConfirmationRepository->getByToken(substr($this->request->query('token'), 0, 32));

        if ($confirmation === null) {
            return new HtmlContent('login/cancel', ['success' => false]);
        }

        \Container::$dbConnection->startTransaction();

        $this->pdm->deleteFromDb($confirmation);

        $user = $this->userRepository->getById($confirmation->getUserId());

        $this->pdm->deleteFromDb($user);

        \Container::$dbConnection->commit();

        return new HtmlContent('login/cancel', ['success' => true]);
    }

    public function requestPasswordReset(): IContent
    {
        if ($this->request->user() !== null) {
            return new JsonContent([
                'redirect' => [
                    'target' => '/' . \Container::$routeCollection->getRoute('home')->generateLink()
                ]
            ]);
        }

        $user = $this->userRepository->getByEmail($this->request->post('email'));

        if ($user === null) {
            return new JsonContent([
                'error' => [
                    'errorText' => 'No user found with the given email address. You can <a href="/signup" title="Sign up">sign up</a>!'
                ]
            ]);
        }

        if (!$user->getActive()) {
            $this->resendConfirmationEmail($user);

            return new JsonContent([
                'error' => [
                    'errorText' => 'User found with the given email address, but the account is not activated. ' .
                        'Please check your email and click on the activation link!'
                ]
            ]);
        }

        $existingResetter = $this->userPasswordResetterRepository->getByUser($user);

        if ($existingResetter !== null && $existingResetter->getExpiresDate() > new DateTime()) {
            return new JsonContent([
                'error' => [
                    'errorText' => 'Password reset was recently requested for this account. Please check your email, or try again later!'
                ]
            ]);
        }

        $token = bin2hex(random_bytes(16));
        $expires = new DateTime('+1 hour');

        $passwordResetter = new UserPasswordResetter();
        $passwordResetter->setUser($user);
        $passwordResetter->setToken($token);
        $passwordResetter->setExpiresDate($expires);

        \Container::$dbConnection->startTransaction();

        if ($existingResetter !== null) {
            $this->pdm->deleteFromDb($existingResetter);
        }

        $this->pdm->saveToDb($passwordResetter);

        \Container::$dbConnection->commit();

        $this->sendPasswordResetEmail($user->getEmail(), $token, $expires);

        return new JsonContent(['success' => true]);
    }


    public function resetPassword(): IContent
    {
        if ($this->request->user() !== null) {
            return new JsonContent([
                'redirect' => [
                    'target' => '/' . \Container::$routeCollection->getRoute('home')->generateLink()
                ]
            ]);
        }

        $token = $this->request->query('token');
        $resetter = $this->userPasswordResetterRepository->getByToken($token);

        if ($resetter === null || $resetter->getExpiresDate() < new DateTime()) {
            return new JsonContent([
                'redirect' => [
                    'target' => '/' . \Container::$routeCollection->getRoute('password-reset')->generateLink(['token' => $token])
                ]
            ]);
        }

        if (strlen($this->request->post('password')) < 6) {
            return new JsonContent([
                'error' => [
                    'errorText' => 'The given password is too short. Please choose a password that is at least 6 characters long!'
                ]
            ]);
        }

        if ($this->request->post('password') !== $this->request->post('password_confirm')) {
            return new JsonContent(['error' => ['errorText' => 'The given passwords do not match.']]);
        }

        \Container::$dbConnection->startTransaction();

        $this->pdm->deleteFromDb($resetter);

        $user = $this->userRepository->getById($resetter->getUserId());
        $user->setPlainPassword($this->request->post('password'));

        $this->pdm->saveToDb($user);

        \Container::$dbConnection->commit();

        $this->request->setUser($user);

        return new JsonContent(['success' => true]);
    }

    private function sendConfirmationEmail(string $email, string $token, DateTime $created): void
    {
        $mail = new Mail();
        $mail->addRecipient($email);
        $mail->setSubject('Welcome to ' . $_ENV['APP_NAME'] . ' - Activate your account');
        $mail->setBodyFromTemplate('signup', [
            'EMAIL' => $email,
            'ACTIVATE_LINK' => $this->request->getBase() . '/' .
                \Container::$routeCollection->getRoute('signup.activate')->generateLink(['token' => $token]),
            'CANCEL_LINK' => $this->request->getBase() . '/' .
                \Container::$routeCollection->getRoute('signup.cancel')->generateLink(['token' => $token]),
            'ACTIVATABLE_UNTIL' => (clone $created)->add(new DateInterval('P1D'))->format('Y-m-d H:i T')
        ]);
        $mail->send();
    }

    private function resendConfirmationEmail(User $user): bool
    {
        $confirmation = $this->userConfirmationRepository->getByUser($user);

        if ($confirmation === null || (clone $confirmation->getLastSentDate())->add(new DateInterval('PT1H')) > new DateTime()) {
            return false;
        }

        $confirmation->setLastSentDate(new DateTime());

        $this->pdm->saveToDb($confirmation);

        $this->sendConfirmationEmail($user->getEmail(), $confirmation->getToken(), $user->getCreatedDate());

        return true;
    }

    private function sendWelcomeEmail(string $email): void
    {
        $mail = new Mail();
        $mail->addRecipient($email);
        $mail->setSubject('Welcome to ' . $_ENV['APP_NAME']);
        $mail->setBodyFromTemplate('signup-noconfirm', [
            'EMAIL' => $email,
        ]);
        $mail->send();
    }

    private function sendPasswordResetEmail(string $email, string $token, DateTime $expires): void
    {
        $mail = new Mail();
        $mail->addRecipient($email);
        $mail->setSubject($_ENV['APP_NAME'] . ' - Password reset');
        $mail->setBodyFromTemplate('password-reset', [
            'EMAIL' => $email,
            'RESET_LINK' => $this->request->getBase() . '/' .
                \Container::$routeCollection->getRoute('password-reset')->generateLink(['token' => $token]),
            'EXPIRES' => $expires->format('Y-m-d H:i T')
        ]);
        $mail->send();
    }
}
