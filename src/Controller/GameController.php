<?php namespace MapGuesser\Controller;

use MapGuesser\Interfaces\Request\IRequest;
use MapGuesser\Response\HtmlContent;
use MapGuesser\Response\JsonContent;
use MapGuesser\Interfaces\Response\IContent;
use MapGuesser\Repository\MapRepository;

class GameController
{
    private IRequest $request;

    private MapRepository $mapRepository;

    public function __construct(IRequest $request)
    {
        $this->request = $request;
        $this->mapRepository = new MapRepository();
    }

    public function getGame(): IContent
    {
        $mapId = (int) $this->request->query('mapId');

        return new HtmlContent('game', $this->prepareGame($mapId));
    }

    public function getGameJson(): IContent
    {
        $mapId = (int) $this->request->query('mapId');

        return new JsonContent($this->prepareGame($mapId));
    }

    private function prepareGame(int $mapId): array
    {
        $map = $this->mapRepository->getById($mapId);

        $session = $this->request->session();

        if (!($state = $session->get('state')) || $state['mapId'] !== $mapId) {
            $session->set('state', [
                'mapId' => $mapId,
                'area' => $map->getArea(),
                'rounds' => []
            ]);
        }

        return ['mapId' => $mapId, 'mapName' => $map->getName(), 'bounds' => $map->getBounds()->toArray()];
    }
}
