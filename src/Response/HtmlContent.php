<?php namespace MapGuesser\Response;

use MapGuesser\View\Linker;

class HtmlContent extends ContentBase
{
    private string $view;

    public function __construct(string $view, array $data = [])
    {
        $this->view = $view;
        $this->data = $data;
    }

    public function render(): void
    {
        if (!empty($_ENV['DEV'])) {
            $generator = new Linker($this->view);
            $generator->generate();
        }

        extract($this->data);

        require ROOT . '/cache/views/' . $this->view . '.php';

        // @phpstan-ignore-next-line - SCRIPT_STARTED is defined in main.php
        echo '<!-- __debug__runtime: ' . round((hrtime(true) - SCRIPT_STARTED) / 1e+6, 1) . ' -->';
    }

    public function getContentType(): string
    {
        return 'text/html';
    }
}
