<?php namespace MapGuesser\Response;

class JsonContent extends ContentBase
{
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function render(): void
    {
        // @phpstan-ignore-next-line - SCRIPT_STARTED is defined in main.php
        $this->data['__debug__runtime'] = round((hrtime(true) - SCRIPT_STARTED) / 1e+6, 1);

        echo json_encode($this->data);
    }

    public function getContentType(): string
    {
        return 'application/json';
    }
}
