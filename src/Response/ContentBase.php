<?php namespace MapGuesser\Response;

use MapGuesser\Interfaces\Response\IContent;

abstract class ContentBase implements IContent
{
    protected array $data;

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    abstract public function render(): void;

    abstract public function getContentType(): string;
}
