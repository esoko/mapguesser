<?php namespace MapGuesser\Cli;

use DateTime;
use MapGuesser\Database\Query\Modify;
use MapGuesser\Database\Query\Select;
use MapGuesser\Interfaces\Database\IResultSet;
use MapGuesser\PersistentData\PersistentDataManager;
use MapGuesser\Repository\UserConfirmationRepository;
use MapGuesser\Repository\UserPasswordResetterRepository;
use MapGuesser\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MaintainDatabaseCommand extends Command
{
    private PersistentDataManager $pdm;

    private UserRepository $userRepository;

    private UserConfirmationRepository $userConfirmationRepository;

    private UserPasswordResetterRepository $userPasswordResetterRepository;

    public function __construct()
    {
        parent::__construct();

        $this->pdm = new PersistentDataManager();
        $this->userRepository = new UserRepository();
        $this->userConfirmationRepository = new UserConfirmationRepository();
        $this->userPasswordResetterRepository = new UserPasswordResetterRepository();
    }

    public function configure(): void
    {
        $this->setName('db:maintain')
            ->setDescription('Maintain database.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->deleteInactiveExpiredUsers();
            $this->deleteExpiredPasswordResetters();
            $this->deleteExpiredSessions();
        } catch (\Exception $e) {
            $output->writeln('<error>Maintenance failed!</error>');
            $output->writeln('');

            $output->writeln((string) $e);
            $output->writeln('');

            return 1;
        }

        $output->writeln('<info>Maintenance was successful!</info>');
        $output->writeln('');

        return 0;
    }

    private function deleteInactiveExpiredUsers(): void
    {
        \Container::$dbConnection->startTransaction();

        foreach ($this->userRepository->getAllInactiveExpired() as $user) {
            //TODO: these can be in some wrapper class
            $userConfirmation = $this->userConfirmationRepository->getByUser($user);
            if ($userConfirmation !== null) {
                $this->pdm->deleteFromDb($userConfirmation);
            }

            $userPasswordResetter = $this->userPasswordResetterRepository->getByUser($user);
            if ($userPasswordResetter !== null) {
                $this->pdm->deleteFromDb($userPasswordResetter);
            }

            $this->pdm->deleteFromDb($user);
        }

        \Container::$dbConnection->commit();
    }

    private function deleteExpiredPasswordResetters(): void
    {
        foreach ($this->userPasswordResetterRepository->getAllExpired() as $passwordResetter) {
            $this->pdm->deleteFromDb($passwordResetter);
        }
    }

    private function deleteExpiredSessions(): void
    {
        //TODO: model may be used for sessions too
        $select = new Select(\Container::$dbConnection, 'sessions');
        $select->columns(['id']);
        $select->where('updated', '<', (new DateTime('-7 days'))->format('Y-m-d H:i:s'));

        $result = $select->execute();

        while ($session = $result->fetch(IResultSet::FETCH_ASSOC)) {
            $modify = new Modify(\Container::$dbConnection, 'sessions');
            $modify->setId($session['id']);
            $modify->delete();
        }
    }
}
