<?php namespace MapGuesser\Cli;

use FilesystemIterator;
use MapGuesser\View\Linker;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LinkViewCommand extends Command
{
    public function configure(): void
    {
        $this->setName('view:link')
            ->setDescription('Linking of views.')
            ->addArgument('view', InputArgument::OPTIONAL, 'View file to be linked.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $views = [];

        $view = $input->getArgument('view');

        if ($view !== null) {
            $views[] = $view;
        } else {
            $folder = ROOT . '/views';
            $folderLength = strlen($folder) + 1;

            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($iterator as $file) {
                if ($file->isDir() || $file->getExtension() !== 'php') {
                    continue;
                }

                $view = substr($file->getPath(), $folderLength) . '/' . $file->getBasename('.php');

                if (strpos($view, 'templates') === 0 || strpos($view, 'tests') === 0) {
                    continue;
                }

                $views[] = $view;
            }
        }

        try {
            foreach ($views as $view) {
                $generator = new Linker($view);
                $generator->generate();
            }
        } catch (\Exception $e) {
            $output->writeln('<error>Linking view(s) failed!</error>');
            $output->writeln('');

            $output->writeln((string) $e);
            $output->writeln('');

            return 1;
        }

        $output->writeln('<info>View(s) successfully linked!</info>');

        return 0;
    }
}
