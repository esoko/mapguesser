<?php namespace MapGuesser\View;

class ParsedFragment
{
    private ?string $extends;

    private array $css;

    private array $js;

    private array $sections;

    private array $extra;

    public function __construct(?string $extends, array $css, array $js, array $sections, array $extra)
    {
        $this->extends = $extends;
        $this->css = $css;
        $this->js = $js;
        $this->sections = $sections;
        $this->extra = $extra;
    }

    public function getExtends(): ?string
    {
        return $this->extends;
    }

    public function getCss(): array
    {
        return $this->css;
    }

    public function getJs(): array
    {
        return $this->js;
    }

    public function getSections(): array
    {
        return $this->sections;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }
}
