<?php namespace MapGuesser\Util\Panorama;

class Pov
{
    private float $heading;

    private float $pitch;

    private float $zoom;

    public function __construct(float $heading, float $pitch, float $zoom)
    {
        $this->heading = $heading;
        $this->pitch = $pitch;
        $this->zoom = $zoom;
    }

    public function setHeading(float $heading): void
    {
        $this->heading = $heading;
    }

    public function setPitch(float $pitch): void
    {
        $this->pitch = $pitch;
    }

    public function setZoom(float $zoom): void
    {
        $this->zoom = $zoom;
    }

    public function getHeading(): float
    {
        return $this->heading;
    }

    public function getPitch(): float
    {
        return $this->pitch;
    }

    public function getZoom(): float
    {
        return $this->zoom;
    }

    public function toArray(): array
    {
        return [
            'heading' => $this->heading,
            'pitch' => $this->pitch,
            'zoom' => $this->zoom
        ];
    }
}
