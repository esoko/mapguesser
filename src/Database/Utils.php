<?php namespace MapGuesser\Database;

class Utils {
    public static function backtick(string $name): string
    {
        return '`' . $name . '`';
    }
}
