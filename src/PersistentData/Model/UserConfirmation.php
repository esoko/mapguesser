<?php namespace MapGuesser\PersistentData\Model;

use DateTime;

class UserConfirmation extends Model
{
    protected static string $table = 'user_confirmations';

    protected static array $fields = ['user_id', 'token', 'last_sent'];

    protected static array $relations = ['user' => User::class];

    private ?User $user = null;

    private ?int $userId = null;

    private string $token = '';

    private DateTime $lastSent;

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setLastSentDate(DateTime $lastSent): void
    {
        $this->lastSent = $lastSent;
    }

    public function setLastSent(string $lastSent): void
    {
        $this->lastSent = new DateTime($lastSent);
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getLastSentDate(): DateTime
    {
        return $this->lastSent;
    }

    public function getLastSent(): string
    {
        return $this->lastSent->format('Y-m-d H:i:s');
    }
}
