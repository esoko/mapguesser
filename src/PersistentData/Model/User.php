<?php namespace MapGuesser\PersistentData\Model;

use DateTime;
use MapGuesser\Interfaces\Authentication\IUser;

class User extends Model implements IUser
{
    protected static string $table = 'users';

    protected static array $fields = ['email', 'password', 'type', 'active', 'google_sub', 'created'];

    private static array $types = ['user', 'admin'];

    private string $email = '';

    private ?string $password = null;

    private string $type = 'user';

    private bool $active = false;

    private ?string $googleSub = null;

    private DateTime $created;

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setPassword(?string $hashedPassword): void
    {
        $this->password = $hashedPassword;
    }

    public function setPlainPassword(string $plainPassword): void
    {
        $this->password = password_hash($plainPassword, PASSWORD_BCRYPT);
    }

    public function setType(string $type): void
    {
        if (in_array($type, self::$types)) {
            $this->type = $type;
        }
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function setGoogleSub(?string $googleSub): void
    {
        $this->googleSub = $googleSub;
    }

    public function setCreatedDate(DateTime $created): void
    {
        $this->created = $created;
    }

    public function setCreated(string $created): void
    {
        $this->created = new DateTime($created);
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getGoogleSub(): ?string
    {
        return $this->googleSub;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->created;
    }

    public function getCreated(): string
    {
        return $this->created->format('Y-m-d H:i:s');
    }

    public function hasPermission(int $permission): bool
    {
        switch ($permission) {
            case IUser::PERMISSION_NORMAL:
                return true;
            case IUser::PERMISSION_ADMIN:
                return $this->type === 'admin';
            default:
                throw new \Exception('Permission does not exist: ' . $permission);
        }
    }

    public function getUniqueId()
    {
        return $this->id;
    }

    public function getDisplayName(): string
    {
        return $this->email;
    }

    public function checkPassword(string $password): bool
    {
        if ($this->password === null) {
            return false;
        }

        return password_verify($password, $this->password);
    }
}
