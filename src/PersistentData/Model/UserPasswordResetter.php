<?php namespace MapGuesser\PersistentData\Model;

use DateTime;

class UserPasswordResetter extends Model
{
    protected static string $table = 'user_password_resetters';

    protected static array $fields = ['user_id', 'token', 'expires'];

    protected static array $relations = ['user' => User::class];

    private ?User $user = null;

    private ?int $userId = null;

    private string $token = '';

    private DateTime $expires;

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setExpiresDate(DateTime $expires): void
    {
        $this->expires = $expires;
    }

    public function setExpires(string $expires): void
    {
        $this->expires = new DateTime($expires);
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpiresDate(): DateTime
    {
        return $this->expires;
    }

    public function getExpires(): string
    {
        return $this->expires->format('Y-m-d H:i:s');
    }
}
