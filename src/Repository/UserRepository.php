<?php namespace MapGuesser\Repository;

use DateTime;
use Generator;
use MapGuesser\Database\Query\Select;
use MapGuesser\PersistentData\Model\User;
use MapGuesser\PersistentData\PersistentDataManager;

class UserRepository
{
    private PersistentDataManager $pdm;

    public function __construct()
    {
        $this->pdm = new PersistentDataManager();
    }

    public function getById(int $userId): ?User
    {
        return $this->pdm->selectFromDbById($userId, User::class);
    }

    public function getByEmail(string $email): ?User
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('email', '=', $email);

        return $this->pdm->selectFromDb($select, User::class);
    }

    public function getByGoogleSub(string $sub): ?User
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('google_sub', '=', $sub);

        return $this->pdm->selectFromDb($select, User::class);
    }

    public function getAllInactiveExpired(): Generator
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('active', '=', false);
        $select->where('created', '<', (new DateTime('-1 day'))->format('Y-m-d H:i:s'));

        yield from $this->pdm->selectMultipleFromDb($select, User::class);
    }
}
