<?php namespace MapGuesser\Repository;

use DateTime;
use Generator;
use MapGuesser\Database\Query\Select;
use MapGuesser\PersistentData\Model\User;
use MapGuesser\PersistentData\Model\UserPasswordResetter;
use MapGuesser\PersistentData\PersistentDataManager;

class UserPasswordResetterRepository
{
    private PersistentDataManager $pdm;

    public function __construct()
    {
        $this->pdm = new PersistentDataManager();
    }

    public function getById(int $userConfirmationId): ?UserPasswordResetter
    {
        return $this->pdm->selectFromDbById($userConfirmationId, UserPasswordResetter::class);
    }

    public function getByToken(string $token): ?UserPasswordResetter
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('token', '=', $token);

        return $this->pdm->selectFromDb($select, UserPasswordResetter::class);
    }

    public function getByUser(User $user): ?UserPasswordResetter
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('user_id', '=', $user->getId());

        return $this->pdm->selectFromDb($select, UserPasswordResetter::class);
    }

    public function getAllExpired(): Generator
    {
        $select = new Select(\Container::$dbConnection);
        $select->where('expires', '<', (new DateTime())->format('Y-m-d H:i:s'));

        yield from $this->pdm->selectMultipleFromDb($select, UserPasswordResetter::class);
    }
}
