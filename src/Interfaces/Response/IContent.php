<?php namespace MapGuesser\Interfaces\Response;

interface IContent
{
    public function setData(array $data): void;

    public function getData(): array;

    public function render(): void;

    public function getContentType(): string;
}
