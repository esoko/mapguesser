<?php namespace MapGuesser\Interfaces\Http;

interface IResponse
{
    public function getBody(): string;

    public function getHeaders(): array;
}
